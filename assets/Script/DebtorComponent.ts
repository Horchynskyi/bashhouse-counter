import EditBox = cc.EditBox;
import Label = cc.Label;

const {ccclass, property} = cc._decorator;

@ccclass
export default class DebtorComponent extends cc.Component {
    @property(EditBox)
    private nameEditBox: EditBox = null;

    @property(EditBox)
    private additionalDebtEditBox: EditBox = null;

    @property(Label)
    private finalDebtLabel: Label = null;

    onLoad () {
        const {additionalDebtEditBox, node} = this;

        additionalDebtEditBox.node.on('text-changed', () => {
            node.emit('additional-debt-changed');
        });
    }

    destroy() { // Calls from editor
        const {node} = this;

        const value = super.destroy();

        node.emit('destroyed');
        node.destroy();

        return value;
    }

    get additionalDebt() {
        const {additionalDebtEditBox} = this;

        return +additionalDebtEditBox.string;
    }

    set finalDebt(value: number) {
        const {finalDebtLabel} = this;

        finalDebtLabel.string = value.toString();
    }
}
