import DebtorsComponent from "./DebtorsComponent";
import EditBox = cc.EditBox;

const {ccclass, property} = cc._decorator;

@ccclass
export default class CalculateComponent extends cc.Component {
    @property(DebtorsComponent)
    private debtorsComponent: DebtorsComponent = null;

    @property(EditBox)
    private checkEditBox: EditBox = null;

    onLoad () {
        const {checkEditBox, debtorsComponent} = this;

        checkEditBox.node.on('text-changed', this.calculate, this);
        debtorsComponent.node.on('debtors-data-changed', this.calculate, this);
    }

    allAdditionalDebtsReducer(allAdditionalDebts, additionalDebt) {
        return allAdditionalDebts + additionalDebt;
    }

    calculate() {
        const {debtorsComponent, checkEditBox} = this;

        const checkValue = +checkEditBox.string;

        const finalDebtWithoutAdditionalDebts = checkValue - debtorsComponent.debtorsComponents.map((debtorComponent) => debtorComponent.additionalDebt).reduce(this.allAdditionalDebtsReducer, 0);

        const amountOfDebtors = debtorsComponent.amountOfDebtors;

        const everyDebtorDebt = +(finalDebtWithoutAdditionalDebts / amountOfDebtors).toFixed(1);

        let final = 0;

        for (let i = 0; i < debtorsComponent.debtorsComponents.length; i++) {
            const debtorComponent = debtorsComponent.debtorsComponents[i];

            const v = everyDebtorDebt + debtorComponent.additionalDebt;

            debtorComponent.finalDebt = v;

            final += v;
        }
    }
}
