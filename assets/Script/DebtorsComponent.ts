import Prefab = cc.Prefab;
import instantiate = cc.instantiate;
import DebtorComponent from "./DebtorComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class DebtorsComponent extends cc.Component {
    @property(Prefab)
    private debtorPrefab: Prefab = null;

    private _debtorsComponents: DebtorComponent[] = null;

    onLoad () {
        this._debtorsComponents = [];
    }

    private addDebtor() { // Calls from editor
        const {node} = this;

        const debtor = instantiate(this.debtorPrefab);

        debtor.on('additional-debt-changed', this.onDebtorsDataChanged, this);

        const debtorComponent = debtor.getComponent(DebtorComponent);

        debtor.once('destroyed', () => {
            this._debtorsComponents.splice(this._debtorsComponents.indexOf(debtorComponent), 1);
            this.onDebtorsDataChanged();
        });

        node.addChild(debtor);

        this._debtorsComponents.push(debtorComponent);

        this.onDebtorsDataChanged();
    }

    onDebtorsDataChanged() {
        const {node} = this;

        node.emit('debtors-data-changed');
    }

    get debtorsComponents() {
        return this._debtorsComponents;
    }

    get amountOfDebtors() {
        return this._debtorsComponents.length;
    }
}
